class AddEmployeeIdToLaborers < ActiveRecord::Migration[6.0]
  def change
    add_reference :laborers, :employee, null: false, foreign_key: true, default: 1
  end
end
