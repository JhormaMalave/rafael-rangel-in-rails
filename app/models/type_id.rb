class TypeId < ApplicationRecord
    has_many :representatives
    has_many :employee
    has_many :students
    
end
