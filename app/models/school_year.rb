class SchoolYear < ApplicationRecord
    has_many :subjects
    has_many :sections
    has_many :enrollments
end
