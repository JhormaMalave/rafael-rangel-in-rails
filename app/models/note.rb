class Note < ApplicationRecord
  belongs_to :teacher
  belongs_to :enrollment
  belongs_to :lapse

  before_validation :generate_enrollment_id
  before_validation :generate_teacher_id

  attr_accessor :type_id_id
  attr_accessor :id_number
  attr_accessor :subject_id
  attr_accessor :section_id

  validates :name, presence: { message: '"La nota no puede quedar vacio"'},
  numericality: { only_integer: true, message: '"La nota debe de ser numerico"'},
  inclusion: { in: (0..20), message: '"La nota no puede ser menor de 0 ni mayor de 20 "' }
  validates_uniqueness_of :enrollment, scope: [:teacher,:lapse], message: '"Ya fue registrada la nota de este estudiante en esta asignatura, sección y lapso"'


  def generate_enrollment_id
    enrollment_search = Enrollment.find_by(student_id: Student.find_by(type_id_id: type_id_id, id_number: id_number), section_id: section_id)
    if enrollment_search != nil
      self.enrollment_id = enrollment_search.id
    end
  end    

  def generate_teacher_id
    teacher_search = Teacher.find_by(subject_id: subject_id, section_id: section_id)
    if teacher_search != nil
      self.teacher_id = teacher_search.id
    end
  end

end
