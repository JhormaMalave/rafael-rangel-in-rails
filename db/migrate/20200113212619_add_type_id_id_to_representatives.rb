class AddTypeIdIdToRepresentatives < ActiveRecord::Migration[6.0]
  def change
    add_reference :representatives, :type_id, null: false, foreign_key: true, default: 1
  end
end
