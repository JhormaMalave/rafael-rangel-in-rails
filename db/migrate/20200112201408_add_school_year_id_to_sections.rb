class AddSchoolYearIdToSections < ActiveRecord::Migration[6.0]
  def change
    add_reference :sections, :school_year, null: false, foreign_key: true, default: 1
  end
end
