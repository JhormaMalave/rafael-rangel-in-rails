class AddWorkingTimeToLaborers < ActiveRecord::Migration[6.0]
  def change
    add_column :laborers, :working_time, :integer
  end
end
