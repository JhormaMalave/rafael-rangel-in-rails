json.extract! note, :id, :name, :teacher_id, :enrollment_id, :lapse_id, :created_at, :updated_at
json.url note_url(note, format: :json)
