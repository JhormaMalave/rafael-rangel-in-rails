class CreateSchoolYears < ActiveRecord::Migration[6.0]
  def change
    create_table :school_years do |t|
      t.integer :name

      t.timestamps
    end
  end
end
