class CreateLapses < ActiveRecord::Migration[6.0]
  def change
    create_table :lapses do |t|
      t.integer :name

      t.timestamps
    end
  end
end
