class Subject < ApplicationRecord
    belongs_to :school_year
    belongs_to :school_period
    has_many :teachers

    validates :name, presence: { message: '"El nombre de la asignatura no puede quedar vacio"'},
    length: { in: 3..30 , message: '"El nombre de la asignatura no puede ser menor a (3) ni mayor a (30) caracteres"'},
    uniqueness: { scope: [:school_year, :school_period], message: '"El nombre de la asignatura ya fue registrada en este periodo escolar y año escolar"' }

    before_validation :capitalize_name

    def capitalize_name
        self.name = self.name.capitalize
    end
end
