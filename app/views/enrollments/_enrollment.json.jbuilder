json.extract! enrollment, :id, :school_year_id, :section_id, :student_id, :school_period_id, :created_at, :updated_at
json.url enrollment_url(enrollment, format: :json)
