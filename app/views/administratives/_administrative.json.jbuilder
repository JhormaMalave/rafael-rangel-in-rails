json.extract! administrative, :id, :description, :created_at, :updated_at
json.url administrative_url(administrative, format: :json)
