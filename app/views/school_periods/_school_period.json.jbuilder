json.extract! school_period, :id, :name, :created_at, :updated_at
json.url school_period_url(school_period, format: :json)
