class SchoolPeriod < ApplicationRecord
    has_many :subjects
    has_many :sections
    has_many :laborers
    has_many :administratives
    has_many :enrollments

    validates :name, presence: { message: '"El período escolar no puede quedar vacio"'},
    numericality: { only_integer: true, message: '"El período escolar debe de ser numerico"'},
    inclusion: { in: (1936..Date.today.year), message: "\"El período escolar no puede ser menor de 1936 ni mayor de #{Date.today.year} \"" },
    uniqueness: { message: '"El período escolar ya fue registrado"' }
    
end
