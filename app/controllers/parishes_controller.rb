class ParishesController < ApplicationController
  before_action :authenticate_user!

  def index
    @parishes = Parish.all().order('name ASC')
  end
  def show
    @parish = Parish.find(params[:id])
  end
  def by_municipality
    @parishes = Municipality.find(params[:id]).parishes.order('name ASC')
  end


end
