json.extract! student, :id, :id_number, :name, :last_name, :date_of_birth, :gender, :direction, :created_at, :updated_at
json.url student_url(student, format: :json)
