# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_01_23_194531) do

  create_table "administratives", force: :cascade do |t|
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "school_period_id", default: 1, null: false
    t.integer "employee_id", default: 1, null: false
    t.integer "working_time"
    t.index ["employee_id"], name: "index_administratives_on_employee_id"
    t.index ["school_period_id"], name: "index_administratives_on_school_period_id"
  end

  create_table "employees", force: :cascade do |t|
    t.integer "id_number"
    t.string "name"
    t.string "last_name"
    t.date "date_of_birth"
    t.integer "gender"
    t.text "direction"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "type_id_id", default: 1, null: false
    t.integer "parish_id", default: 1, null: false
    t.integer "workstation_id", default: 1, null: false
    t.index ["parish_id"], name: "index_employees_on_parish_id"
    t.index ["type_id_id"], name: "index_employees_on_type_id_id"
    t.index ["workstation_id"], name: "index_employees_on_workstation_id"
  end

  create_table "enrollments", force: :cascade do |t|
    t.integer "school_year_id", null: false
    t.integer "section_id"
    t.integer "student_id", null: false
    t.integer "school_period_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "representative_id"
    t.integer "status"
    t.boolean "repeat"
    t.index ["representative_id"], name: "index_enrollments_on_representative_id"
    t.index ["school_period_id"], name: "index_enrollments_on_school_period_id"
    t.index ["school_year_id"], name: "index_enrollments_on_school_year_id"
    t.index ["section_id"], name: "index_enrollments_on_section_id"
    t.index ["student_id"], name: "index_enrollments_on_student_id"
  end

  create_table "laborers", force: :cascade do |t|
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "school_period_id", default: 1, null: false
    t.integer "employee_id", default: 1, null: false
    t.integer "working_time"
    t.index ["employee_id"], name: "index_laborers_on_employee_id"
    t.index ["school_period_id"], name: "index_laborers_on_school_period_id"
  end

  create_table "lapses", force: :cascade do |t|
    t.integer "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "municipalities", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "notes", force: :cascade do |t|
    t.integer "name"
    t.integer "teacher_id", null: false
    t.integer "enrollment_id", null: false
    t.integer "lapse_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["enrollment_id"], name: "index_notes_on_enrollment_id"
    t.index ["lapse_id"], name: "index_notes_on_lapse_id"
    t.index ["teacher_id"], name: "index_notes_on_teacher_id"
  end

  create_table "parishes", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "municipality_id", default: 1, null: false
    t.index ["municipality_id"], name: "index_parishes_on_municipality_id"
  end

  create_table "representatives", force: :cascade do |t|
    t.integer "id_number"
    t.string "name"
    t.string "last_name"
    t.date "date_of_birth"
    t.integer "gender"
    t.text "direction"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "type_id_id", default: 1, null: false
    t.integer "parish_id", default: 1, null: false
    t.index ["parish_id"], name: "index_representatives_on_parish_id"
    t.index ["type_id_id"], name: "index_representatives_on_type_id_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "school_periods", force: :cascade do |t|
    t.integer "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "school_years", force: :cascade do |t|
    t.integer "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "sections", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "school_period_id", default: 1, null: false
    t.integer "school_year_id", default: 1, null: false
    t.integer "employee_id"
    t.index ["employee_id"], name: "index_sections_on_employee_id"
    t.index ["school_period_id"], name: "index_sections_on_school_period_id"
    t.index ["school_year_id"], name: "index_sections_on_school_year_id"
  end

  create_table "students", force: :cascade do |t|
    t.integer "id_number"
    t.string "name"
    t.string "last_name"
    t.date "date_of_birth"
    t.integer "gender"
    t.text "direction"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "type_id_id", default: 1, null: false
    t.integer "parish_id", default: 1, null: false
    t.index ["parish_id"], name: "index_students_on_parish_id"
    t.index ["type_id_id"], name: "index_students_on_type_id_id"
  end

  create_table "subjects", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "school_year_id", default: 1, null: false
    t.integer "school_period_id", default: 1, null: false
    t.index ["school_period_id"], name: "index_subjects_on_school_period_id"
    t.index ["school_year_id"], name: "index_subjects_on_school_year_id"
  end

  create_table "teachers", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "working_time"
    t.integer "employee_id", default: 1, null: false
    t.integer "section_id", default: 1, null: false
    t.integer "subject_id", default: 1, null: false
    t.index ["employee_id"], name: "index_teachers_on_employee_id"
    t.index ["section_id"], name: "index_teachers_on_section_id"
    t.index ["subject_id"], name: "index_teachers_on_subject_id"
  end

  create_table "type_ids", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "role_id", default: 1, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["role_id"], name: "index_users_on_role_id"
  end

  create_table "workstations", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "administratives", "employees"
  add_foreign_key "administratives", "school_periods"
  add_foreign_key "employees", "parishes"
  add_foreign_key "employees", "type_ids"
  add_foreign_key "employees", "workstations"
  add_foreign_key "enrollments", "representatives"
  add_foreign_key "enrollments", "school_periods"
  add_foreign_key "enrollments", "school_years"
  add_foreign_key "enrollments", "sections"
  add_foreign_key "enrollments", "students"
  add_foreign_key "laborers", "employees"
  add_foreign_key "laborers", "school_periods"
  add_foreign_key "notes", "enrollments"
  add_foreign_key "notes", "lapses"
  add_foreign_key "notes", "teachers"
  add_foreign_key "parishes", "municipalities"
  add_foreign_key "representatives", "parishes"
  add_foreign_key "representatives", "type_ids"
  add_foreign_key "sections", "employees"
  add_foreign_key "sections", "school_periods"
  add_foreign_key "sections", "school_years"
  add_foreign_key "students", "parishes"
  add_foreign_key "students", "type_ids"
  add_foreign_key "subjects", "school_periods"
  add_foreign_key "subjects", "school_years"
  add_foreign_key "teachers", "employees"
  add_foreign_key "teachers", "sections"
  add_foreign_key "teachers", "subjects"
  add_foreign_key "users", "roles"
end
