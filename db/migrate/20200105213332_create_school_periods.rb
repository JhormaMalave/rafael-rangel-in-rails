class CreateSchoolPeriods < ActiveRecord::Migration[6.0]
  def change
    create_table :school_periods do |t|
      t.integer :name, unique: true

      t.timestamps
    end
  end
end
