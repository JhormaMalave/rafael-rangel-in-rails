class SchoolPeriodsController{
    constructor() {
        this.startPeriod = document.getElementById('start-period')
        this.endPeriod = document.getElementById('end-period')
    }

    schoolPeriodsIndex() {
    }

    schoolPeriodsNew () {
    }

    schoolPeriodsEdit () {
    }

    view () {
    }

    getEndPeriodSchool () {        
        console.log(this.startPeriod.value);
        
        if (this.startPeriod.value <= new Date().getFullYear() && this.startPeriod.value >= 1936){
            this.endPeriod.placeholder = parseInt(this.startPeriod.value) + 1;
        }
    }
}

if(document.getElementById('form_school_period')){
    const schoolPeriods = new SchoolPeriodsController()

    schoolPeriods.startPeriod.addEventListener('keyup', schoolPeriods.getEndPeriodSchool)
}