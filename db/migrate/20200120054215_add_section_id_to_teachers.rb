class AddSectionIdToTeachers < ActiveRecord::Migration[6.0]
  def change
    add_reference :teachers, :section, null: false, foreign_key: true, default: 1
  end
end
