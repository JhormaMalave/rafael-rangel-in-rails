json.extract! representative, :id, :id_number, :name, :last_name, :date_of_birth, :gender, :direction, :created_at, :updated_at
json.url representative_url(representative, format: :json)
