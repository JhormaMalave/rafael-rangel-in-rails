class StudentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_student, only: [:show, :edit, :update, :destroy]
  before_action :variables_for_create_and_update, only: [:create, :update]

  # GET /students
  # GET /students.json
  def index
  end

  # GET /students/1
  # GET /students/1.json
  def show
  end

  # GET /students/search?
  def search
    @student = Student.find_by(type_id: params[:type_id], id_number: params[:number_id])
    if @student === nil
      redirect_to students_url, alert: 'No existe representante registrado con ese numero de identidad.' 
    else
      redirect_to @student
    end
  end

  # GET /students/new
  def new
    @student = Student.new
    @municipalities = Municipality.all().order('name ASC')
    @parishes = Municipality.find(20).parishes.order('name ASC')
  end

  # GET /students/1/edit
  def edit
    @municipalities = Municipality.all().order('name ASC')
    @parishes = Municipality.find(@student.parish.municipality.id).parishes.order('name ASC')
  end

  # POST /students
  # POST /students.json
  def create
    @student = Student.new(student_params)

    respond_to do |format|
      if @student.save
        format.html { redirect_to @student, notice: 'Student was successfully created.' }
        format.json { render :show, status: :created, location: @student }
      else
        format.html { render :new }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    respond_to do |format|
      if @student.update(student_params)
        format.html { redirect_to @student, notice: 'Student was successfully updated.' }
        format.json { render :show, status: :ok, location: @student }
      else
        format.html { render :edit }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /student/all
  def all
    @students = Student.all.last(10).reverse

  end

  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    @student.destroy
    respond_to do |format|
      format.html { redirect_to students_url, notice: 'Student was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def student_params
      params.require(:student).permit(:type_id_id, :id_number, :name, :last_name, :date_of_birth, :gender, :parish_id, :direction)
    end

    def variables_for_create_and_update
      @municipalities = Municipality.all().order('name ASC')
      @parishes = Parish.all()
    end
end
