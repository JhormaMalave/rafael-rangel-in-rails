class AddSchoolYearIdToSubjects < ActiveRecord::Migration[6.0]
  def change
    add_reference :subjects, :school_year, null: false, foreign_key: true, default: 1
  end
end
