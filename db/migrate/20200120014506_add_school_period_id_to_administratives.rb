class AddSchoolPeriodIdToAdministratives < ActiveRecord::Migration[6.0]
  def change
    add_reference :administratives, :school_period, null: false, foreign_key: true, default: 1
  end
end
