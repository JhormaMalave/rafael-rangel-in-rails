json.extract! laborer, :id, :description, :created_at, :updated_at
json.url laborer_url(laborer, format: :json)
