class SectionsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_section, only: [:show, :edit, :update, :destroy, :payroll]

  # GET /sections
  # GET /sections.json
  def index
    @section = Section.new
    @school_years = SchoolYear.all 
    if params[:year]
      session[:school_year] = params[:year]
      @sections = Section.where(school_year_id: session[:school_year], school_period_id: session[:school_period] ).order('name ASC')
    else
      if session[:school_year]
        @sections = Section.where(school_year_id: session[:school_year], school_period_id: session[:school_period] ).order('name ASC')
      end
    end

  end

  # GET /sections/1
  # GET /sections/1.json
  def show
  end

  # GET /sections/new
  def new
    @section = Section.new
    @school_years = SchoolYear.all
  end

  # GET /sections/1/edit
  def edit
    @school_years = SchoolYear.all
  end

  # POST /sections
  # POST /sections.json
  def create
    @section = Section.new(section_params)
    @section.school_period_id = session[:school_period]
    respond_to do |format|
      if @section.save
        format.html { redirect_to @section, notice: 'Sección creada exitosamente.' }
        format.json { render :show, status: :created, location: @section }
      else
        format.html { render :new }
        format.json { render json: @section.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sections/1
  # PATCH/PUT /sections/1.json
  def update
    respond_to do |format|
      if @section.update(section_params)
        format.html { redirect_to @section, notice: 'Sección actualizada exitosamente.' }
        format.json { render :show, status: :ok, location: @section }
      else
        format.html { render :edit }
        format.json { render json: @section.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sections/1
  # DELETE /sections/1.json
  def destroy
    @section.destroy
    respond_to do |format|
      format.html { redirect_to sections_url, notice: 'Sección eliminada exitosamente.' }
      format.json { head :no_content }
    end
  end

  def by_year
    @sections = Section.where(school_year_id: params[:id], school_period_id: session[:school_period]).order('name ASC')
  end

  def payroll
    respond_to do |format|
      format.html
      format.json
      format.pdf {render template: 'sections/payroll', pdf: 'Payroll'}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_section
      @section = Section.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def section_params
      params.require(:section).permit(:type_id_id, :id_number, :school_year_id, :name)
    end
end
