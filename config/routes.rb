Rails.application.routes.draw do

  devise_for :users
  root to: 'students#index'

  #REPORTS
  get 'employees/all' => 'employees#all'  
  get 'students/all' => 'students#all'  
  get 'enrollments/status' => 'enrollments#status'
  get 'enrollments/female' => 'enrollments#female'
  get 'enrollments/male' => 'enrollments#male'
  get 'enrollments/repeat' => 'enrollments#repeat'
  get 'enrollments/valera' => 'enrollments#valera'
  get 'enrollments/other_municipalities' => 'enrollments#other_municipalities'


  get 'school_periods/change_school_period/:id' => "school_periods#change_school_period", as: :change_school_period
  get 'representatives/search' => "representatives#search"
  get 'employees/search' => "employees#search"
  get 'students/search' => "students#search"


  #JSON 
  get 'school_years', to: 'school_years#index'
  get 'school_years/:id', to: 'school_years#show'
  get 'parishes', to: 'parishes#index'
  get 'parishes/:id', to: 'parishes#show'
  get 'parishes/municipality/:id', to: 'parishes#by_municipality'
  get 'subjects/year/:id', to: 'subjects#by_year'
  get 'sections/year/:id', to: 'sections#by_year'
  
  #PDF
  get 'enrollments/:id/certificate_of_study', to: 'enrollments#certificate_of_study'
  get 'teachers/:id/payroll', to: 'teachers#payroll'
  get 'sections/:id/payroll', to: 'sections#payroll'

  resources :notes
  resources :enrollments
  resources :teachers
  resources :sections
  resources :administratives
  resources :students
  resources :laborers
  resources :employees
  resources :representatives
  resources :subjects
  resources :school_periods
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
