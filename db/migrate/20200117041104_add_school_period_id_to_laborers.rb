class AddSchoolPeriodIdToLaborers < ActiveRecord::Migration[6.0]
  def change
    add_reference :laborers, :school_period, null: false, foreign_key: true, default: 1
  end
end
