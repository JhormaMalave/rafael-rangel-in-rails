class AddRepeatToEnrollments < ActiveRecord::Migration[6.0]
  def change
    add_column :enrollments, :repeat, :boolean
  end
end
