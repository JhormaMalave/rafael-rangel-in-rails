class Section < ApplicationRecord
    belongs_to :school_year
    belongs_to :school_period
    belongs_to :employee, required: false
    has_many :teachers
    has_many :enrollments

    validates :name, presence: { message: '"El nombre de la sección no puede quedar vacio"'},
    format: { with: /\A[a-hA-H]+\z/, message: "El nombre de la seccion solo debe de tener letras entre la (A) a la (H)" },
    length: { is: 1, message: "El nombre de la sección solo debe de tener una letra" },
    uniqueness: { scope: [:school_year, :school_period], message: '"El nombre de la sección ya fue registrada en este periodo escolar y año escolar"' }

    before_validation :upcase_name
    before_validation :generate_employee_id

    attr_accessor :type_id_id
    attr_accessor :id_number

    def upcase_name
        self.name = self.name.upcase
    end



    def generate_employee_id
        employee_search = Employee.find_by(type_id: type_id_id, id_number: id_number)
        if employee_search != nil
            if employee_search.workstation.id == 1
                self.employee_id = employee_search.id
                validates_uniqueness_of :employee, scope: :school_period, message: '"Este docente ya es guía de una seccíon"'
            end
        end
    end    
end
