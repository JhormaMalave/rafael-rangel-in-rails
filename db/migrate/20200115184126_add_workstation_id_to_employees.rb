class AddWorkstationIdToEmployees < ActiveRecord::Migration[6.0]
  def change
    add_reference :employees, :workstation, null: false, foreign_key: true, default: 1
  end
end
