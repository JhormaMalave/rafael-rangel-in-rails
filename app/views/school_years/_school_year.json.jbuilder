json.extract! school_year, :id, :name, :created_at, :updated_at
json.url school_years_url(school_year, format: :json)
