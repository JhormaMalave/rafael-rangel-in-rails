class EmployeesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_employee, only: [:show, :edit, :update, :destroy]
  before_action :variables_for_create_and_update, only: [:create, :update]

  # GET /employees
  # GET /employees.json
  def index
    @employees = Employee.all.last(5).reverse
    @type_ids = TypeId.all
  end

  # GET /employees/1
  # GET /employees/1.json
  def show
  end

  # GET /employees/new
  def new
    @employee = Employee.new
    @type_ids = TypeId.all
    @municipalities = Municipality.all().order('name ASC')
    @parishes = Municipality.find(20).parishes.order('name ASC')
  end

    # GET /employees/search?
    def search
      @employee = Employee.find_by(type_id: params[:type_id], id_number: params[:number_id])
      if @employee === nil
        redirect_to employees_url, alert: 'No existe representante registrado con ese numero de identidad.' 
      else
        redirect_to @employee
      end
    end
    
  # GET /employees/1/edit
  def edit
    @type_ids = TypeId.all
    @municipalities = Municipality.all().order('name ASC')
    @parishes = Municipality.find(@employee.parish.municipality.id).parishes.order('name ASC')
  end

  # POST /employees
  # POST /employees.json
  def create
    @employee = Employee.new(employee_params)

    respond_to do |format|
      if @employee.save
        format.html { redirect_to @employee, notice: 'Employee was successfully created.' }
        format.json { render :show, status: :created, location: @employee }
      else
        format.html { render :new }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employees/1
  # PATCH/PUT /employees/1.json
  def update
    respond_to do |format|
      if @employee.update(employee_params)
        format.html { redirect_to @employee, notice: 'Employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @employee }
      else
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employees/1
  # DELETE /employees/1.json
  def destroy
    @employee.destroy
    respond_to do |format|
      format.html { redirect_to employees_url, notice: 'Employee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def all
    @employees = Employee.all.last(10).reverse
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Employee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      params.require(:employee).permit(:type_id_id, :id_number, :name, :last_name, :date_of_birth, :gender, :parish_id, :direction, :workstation_id,)
    end

    def variables_for_create_and_update
      @type_ids = TypeId.all
      @municipalities = Municipality.all().order('name ASC')
      @parishes = Parish.all()
    end
end
