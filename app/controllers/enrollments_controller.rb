class EnrollmentsController < ApplicationController
  before_action :set_enrollment, only: [:show, :edit, :update, :destroy, :certificate_of_study]

  # GET /enrollments
  # GET /enrollments.json
  def index
    @enrollments = Enrollment.where(school_period_id: session[:school_period]).last(5).reverse
  end

  # GET /enrollments/1
  # GET /enrollments/1.json
  def show
  end

  # GET /enrollments/new
  def new
    @enrollment = Enrollment.new
  end

  # GET /enrollments/1/edit
  def edit
  end

  # POST /enrollments
  # POST /enrollments.json
  def create
    @enrollment = Enrollment.new(enrollment_params)
    @enrollment.school_period_id = session[:school_period]
    @enrollment.status = 0
    respond_to do |format|
      if @enrollment.save
        format.html { redirect_to @enrollment, notice: 'Enrollment was successfully created.' }
        format.json { render :show, status: :created, location: @enrollment }
      else
        format.html { render :new }
        format.json { render json: @enrollment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /enrollments/1
  # PATCH/PUT /enrollments/1.json
  def update
    respond_to do |format|
      if @enrollment.update(enrollment_params)
        format.html { redirect_to @enrollment, notice: 'Enrollment was successfully updated.' }
        format.json { render :show, status: :ok, location: @enrollment }
      else
        format.html { render :edit }
        format.json { render json: @enrollment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /enrollments/1
  # DELETE /enrollments/1.json
  def destroy
    @enrollment.destroy
    respond_to do |format|
      format.html { redirect_to enrollments_url, notice: 'Enrollment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def certificate_of_study
    respond_to do |format|
      format.html
      format.json
      format.pdf {render template: 'enrollments/certificate_of_study', pdf: 'Certificate_of_study'}
    end
  end

  def status
    @enrollments = Enrollment.where(school_period_id: session[:school_period], status: 1)
    respond_to do |format|
      format.html
      format.json
      format.pdf {render template: 'enrollments/payroll', pdf: 'Retirados'}
    end
  end

  def female
    @enrollments = Enrollment.where(school_period_id: session[:school_period], student: Student.where(gender: 1))
    respond_to do |format|
      format.html
      format.json
      format.pdf {render template: 'enrollments/payroll', pdf: 'Femenino'}
    end
  end

  def male
    @enrollments = Enrollment.where(school_period_id: session[:school_period], student: Student.where(gender: 0))
    respond_to do |format|
      format.html
      format.json
      format.pdf {render template: 'enrollments/payroll', pdf: 'Masculino'}
    end  
  end

  def repeat
    @enrollments = Enrollment.where(school_period_id: session[:school_period], repeat: 1)
    respond_to do |format|
      format.html
      format.json
      format.pdf {render template: 'enrollments/payroll', pdf: 'Repeat'}
    end  
  end

  def valera
    @enrollments = Enrollment.where(school_period_id: session[:school_period], student: Student.where(parish: Parish.where(municipality_id: 20)))
    respond_to do |format|
      format.html
      format.json
      format.pdf {render template: 'enrollments/payroll', pdf: 'Valera'}
    end  
  end

  def other_municipalities
    @enrollments = Enrollment.where(school_period_id: session[:school_period], student: Student.where(parish: Parish.where.not(municipality_id: 20)))
    respond_to do |format|
      format.html
      format.json
      format.pdf {render template: 'enrollments/payroll', pdf: 'Otros_Municipios'}
    end  
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_enrollment
      @enrollment = Enrollment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def enrollment_params
      params.require(:enrollment).permit(:repeat, :status, :representative_type_id_id, :representative_id_number,:type_id_id, :id_number, :school_year_id, :section_id, :student_id, :school_period_id)
    end
end
