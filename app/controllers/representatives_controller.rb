class RepresentativesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_representative, only: [:show, :edit, :update, :destroy]
  before_action :variables_for_create_and_update, only: [:create, :update]

  # GET /representatives
  # GET /representatives.json
  def index
    @representatives = Representative.all.last(5).reverse
  end

  # GET /representatives/1
  # GET /representatives/1.json
  def show
  end

  # GET /representatives/search?
  def search
    @representative = Representative.find_by(type_id: params[:type_id], id_number: params[:number_id])
    if @representative === nil
      redirect_to representatives_url, alert: 'No existe representante registrado con ese numero de identidad.' 
    else
      redirect_to @representative
    end
  end


  # GET /representatives/new
  def new
    @representative = Representative.new
    @municipalities = Municipality.all().order('name ASC')
    @parishes = Municipality.find(20).parishes.order('name ASC')
  end

  # GET /representatives/1/edit
  def edit
    @municipalities = Municipality.all().order('name ASC')
    @parishes = Municipality.find(@representative.parish.municipality.id).parishes.order('name ASC')
  end

  # POST /representatives
  # POST /representatives.json
  def create
    @representative = Representative.new(representative_params)
    

    respond_to do |format|
      if @representative.save
        format.html { redirect_to @representative, notice: 'Representante fue creado exitosamente.' }
        format.json { render :show, status: :created, location: @representative }
      else
        format.html { render :new }
        format.json { render json: @representative.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /representatives/1
  # PATCH/PUT /representatives/1.json
  def update
    respond_to do |format|
      if @representative.update(representative_params)
        format.html { redirect_to @representative, notice: 'Representante fue actualizado exitosamente.' }
        format.json { render :show, status: :ok, location: @representative }
      else
        format.html { render :edit }
        format.json { render json: @representative.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /representatives/1
  # DELETE /representatives/1.json
  def destroy
    @representative.destroy
    respond_to do |format|
      format.html { redirect_to representatives_url, notice: 'Representative fue eliminado exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_representative
      @representative = Representative.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def representative_params
      params.require(:representative).permit(:type_id_id, :id_number, :name, :last_name, :date_of_birth, :gender, :parish_id, :direction)
    end

    def variables_for_create_and_update
      @municipalities = Municipality.all().order('name ASC')
      @parishes = Parish.all()
    end
end
