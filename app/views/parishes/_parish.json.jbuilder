json.extract! parish, :id, :name, :created_at, :updated_at
json.url parishes_url(parish, format: :json)
