# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
SchoolPeriod.find_or_create_by({name: Date.today.year})

lapses = [1,2,3]

rols = ['Administrado', 'Sectional', 'Dep. Evaluacion']

school_year = [1, 2, 3, 4, 5]

type_id = ['V','P']

workstations = ['Docente', 'Administrativo', 'Obrero']

municipalities = [
    'Trujillo',
    'Andrés Bello',
    'Boconó',
    'Bolívar',
    'Candelaria',
    'Carache',
    'Escuque',
    'Jose Felipe Márquez Cañizales',
    'Juan Vicente Campo Elías',
    'La Ceiba',
    'Miranda',
    'Monte Carmelo',
    'Motatán',
    'Pampán',
    'Pampanito',
    'Rafael Rangel',
    'San Rafael de Carvajal',
    'Sucre',
    'Urdaneta',
    'Valera'
]
parishes_from_andres_bello = [
    'Araguaney',
    'El Jaguito',
    'La Esperanza',
    'Santa Isabel'
]
parishes_from_bocono = [
    'Boconó',
    'El carmen',
    'Mosquey',
    'Ayacucho',
    'Burbusay',
    'General Ribas',
    'Guaramacal',
    'Vega de Guaramacal',
    'Monseñor Jáuregui',
    'Rafael Rangel',
    'San Miguel',
    'San José'
]
parishes_from_bolivar = [
    'Sabana Grande',
    'Cheregüé',
    'Granados'
]
parishes_from_candelaria = [
    'Arnoldo Gabaldón',
    'Bolivia',
    'Carrillo',
    'Cegarra',
    'Chejende',
    'Manuel Salvador Ulloa',
    'San José'
]
parishes_from_carache = [
    'Carache',
    'La Concepción',
    'Cuicas',
    'Panamericana',
    'Santa Cruz'
]
parishes_from_escuque = [
    'Escuque',
    'La Unión (El Alto Escuque)',
    'Santa Rita',
    'Sabana Libre'
]
parishes_from_jose_felipe_marquez_cañizales = [
    'El Socorro',
    'Los Caprichos',
    'Antonio José de Sucre'
]
parishes_from_juan_vicente_campos_elias = [
    'Campo Elías',
    'Arnoldo Gabaldón'
]
parishes_from_la_ceiba = [
    'Santa Apolonia',
    'El Progreso',
    'La Ceiba',
    'Tres de Febrero'
]
parishes_from_miranda = [
    'El Dividive',
    'Agua Santa',
    'Agua Caliente',
    'El Cenizo',
    'Valerita',
    'El Salto' 
]
parishes_from_monte_carmelo = [
    'Monte Carmelo',
    'Buena Vista',
    'Santa María del Horcón'
]
parishes_from_motatan = [
    'Motatán',
    'El Baño',
    'Jalisco'
]
parishes_from_pampan = [
    'Pampán',
    'Flor de Patria',
    'La Paz',
    'Santa Ana'
]
parishes_from_pampanito = [
    'Pampanito',
    'La Concepción',
    'Pampanito II'
]
parishes_from_rafael_rangel = [
    'Betijoque',
    'José Gregorio Hernández',
    'La Pueblita',
    'Los Cedros'
]
parishes_from_san_rafael_de_carvajal = [
    'Carvajal',
    'Campo Alegre',
    'Antonio Nicolás Briceño',
    'José Leonardo Suárez'
]
parishes_from_sucre = [
    'Carvajal',
    'Campo Alegre',
    'Antonio Nicolás Briceño',
    'José Leonardo Suárez'
]
parishes_from_trujillo = [
    'Andrés Linares',
    'Chiquinquirá',
    'Cristóbal Mendoza',
    'Cruz Carrillo',
    'Matriz',
    'Monseñor Carrillo',
    'Tres Esquinas'
]
parishes_from_urdaneta = [
    'Cabimbú',
    'Jajó',
    'La Mesa de Esnujaque',
    'Santiago',
    'Tuñame',
    'La Quebrada'
]
parishes_from_valera = [
    'Juan Ignacio Montilla',
    'La Beatriz',
    'La Puerta',
    'Mendoza del Valle de Momboy',
    'Mercedes Díaz',
    'San Luis'
]

municipalities.each do |municipality|
    Municipality.find_or_create_by({name: municipality})
end

parishes_from_andres_bello.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 2})
end
parishes_from_bocono.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 3})
end
parishes_from_bolivar.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 4})
end
parishes_from_candelaria.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 5})
end
parishes_from_carache.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 6})
end
parishes_from_escuque.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 7})
end
parishes_from_jose_felipe_marquez_cañizales.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 8})
end
parishes_from_juan_vicente_campos_elias.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 9})
end
parishes_from_la_ceiba.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 10})
end
parishes_from_miranda.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 11})
end
parishes_from_monte_carmelo.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 12})
end
parishes_from_motatan.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 13})
end
parishes_from_pampan.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 14})
end
parishes_from_pampanito.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 15})
end
parishes_from_rafael_rangel.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 16})
end
parishes_from_san_rafael_de_carvajal.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 17})
end
parishes_from_sucre.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 18})
end
parishes_from_trujillo.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 1})
end
parishes_from_urdaneta.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 19})
end
parishes_from_valera.each do |parish|
    Parish.find_or_create_by({name: parish, municipality_id: 20})
end

type_id.each do |type|
    TypeId.find_or_create_by({name: type})
end

school_year.each do |year|
    SchoolYear.find_or_create_by({name: year})
end

workstations.each do |workstation|
    Workstation.find_or_create_by({name: workstation})
end

rols.each do |role|
    Role.find_or_create_by({name: role})
end

lapses.each do |lapse|
    Lapse.find_or_create_by({name: lapse})
end