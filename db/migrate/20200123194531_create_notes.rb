class CreateNotes < ActiveRecord::Migration[6.0]
  def change
    create_table :notes do |t|
      t.integer :name
      t.references :teacher, null: false, foreign_key: true
      t.references :enrollment, null: false, foreign_key: true
      t.references :lapse, null: false, foreign_key: true

      t.timestamps
    end
  end
end
