class Laborer < ApplicationRecord
    belongs_to :school_period
    belongs_to :employee

    enum working_time: [:Mañana, :Tarde, :Ambos ]

    validates :description, presence: { message: '"Ingresa una descripción de la carga laboral obrera"'}
    validates_uniqueness_of :employee_id, scope: :school_period, message: '"Este empleado ya posee una carga laboral obrera"'
    validates :working_time, presence: { message: '"El turno de trabajo no puede quedar vacio"'}

    attr_accessor :type_id_id
    attr_accessor :id_number


    def generate_employee_id
        employee_search = Employee.find_by(type_id: type_id_id, id_number: id_number)
        if employee_search != nil
            if employee_search.workstation.id == 3
                employee_search.id
            end
        end
    end    
end
