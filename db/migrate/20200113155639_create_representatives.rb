class CreateRepresentatives < ActiveRecord::Migration[6.0]
  def change
    create_table :representatives do |t|
      t.integer :id_number
      t.string :name
      t.string :last_name
      t.date :date_of_birth
      t.integer :gender
      t.text :direction

      t.timestamps
    end
  end
end
