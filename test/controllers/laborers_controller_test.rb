require 'test_helper'

class LaborersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @laborer = laborers(:one)
  end

  test "should get index" do
    get laborers_url
    assert_response :success
  end

  test "should get new" do
    get new_laborer_url
    assert_response :success
  end

  test "should create laborer" do
    assert_difference('Laborer.count') do
      post laborers_url, params: { laborer: { description: @laborer.description } }
    end

    assert_redirected_to laborer_url(Laborer.last)
  end

  test "should show laborer" do
    get laborer_url(@laborer)
    assert_response :success
  end

  test "should get edit" do
    get edit_laborer_url(@laborer)
    assert_response :success
  end

  test "should update laborer" do
    patch laborer_url(@laborer), params: { laborer: { description: @laborer.description } }
    assert_redirected_to laborer_url(@laborer)
  end

  test "should destroy laborer" do
    assert_difference('Laborer.count', -1) do
      delete laborer_url(@laborer)
    end

    assert_redirected_to laborers_url
  end
end
