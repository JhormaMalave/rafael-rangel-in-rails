require "application_system_test_case"

class AdministrativesTest < ApplicationSystemTestCase
  setup do
    @administrative = administratives(:one)
  end

  test "visiting the index" do
    visit administratives_url
    assert_selector "h1", text: "Administratives"
  end

  test "creating a Administrative" do
    visit administratives_url
    click_on "New Administrative"

    fill_in "Description", with: @administrative.description
    click_on "Create Administrative"

    assert_text "Administrative was successfully created"
    click_on "Back"
  end

  test "updating a Administrative" do
    visit administratives_url
    click_on "Edit", match: :first

    fill_in "Description", with: @administrative.description
    click_on "Update Administrative"

    assert_text "Administrative was successfully updated"
    click_on "Back"
  end

  test "destroying a Administrative" do
    visit administratives_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Administrative was successfully destroyed"
  end
end
