class AddParishIdToEmployees < ActiveRecord::Migration[6.0]
  def change
    add_reference :employees, :parish, null: false, foreign_key: true, default: 1
  end
end
