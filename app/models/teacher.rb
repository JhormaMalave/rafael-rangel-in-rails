class Teacher < ApplicationRecord
    belongs_to :employee
    belongs_to :section
    belongs_to :subject
    has_many :notes

    enum working_time: [:Mañana, :Tarde, :Ambos ]

    validates_uniqueness_of :section_id, scope: :subject, message: '"Este cargo laboral ya se encuentra ocupado"'
    validates :working_time, presence: { message: '"El turno de trabajo no puede quedar vacio"'}

    attr_accessor :type_id_id
    attr_accessor :id_number


    def generate_employee_id
        employee_search = Employee.find_by(type_id: type_id_id, id_number: id_number)
        if employee_search != nil
            if employee_search.workstation.id == 1
                employee_search.id
            end
        end
    end    
end
