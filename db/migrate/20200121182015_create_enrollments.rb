class CreateEnrollments < ActiveRecord::Migration[6.0]
  def change
    create_table :enrollments do |t|
      t.references :school_year, null: false, foreign_key: true
      t.references :section, null: true, foreign_key: true
      t.references :student, null: false, foreign_key: true
      t.references :school_period, null: false, foreign_key: true

      t.timestamps
    end
  end
end
