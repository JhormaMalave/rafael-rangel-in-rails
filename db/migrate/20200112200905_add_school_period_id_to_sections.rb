class AddSchoolPeriodIdToSections < ActiveRecord::Migration[6.0]
  def change
    add_reference :sections, :school_period, null: false, foreign_key: true, default: 1
  end
end
