class AddTypeIdIdToStudents < ActiveRecord::Migration[6.0]
  def change
    add_reference :students, :type_id, null: false, foreign_key: true, default: 1
  end
end
