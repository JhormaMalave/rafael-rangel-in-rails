class Representative < ApplicationRecord
    belongs_to :type_id
    belongs_to :parish
    has_many :enrollments

    enum gender: [:Masculino, :Femenino ]
    

    validates :id_number, presence: { message: '"El numero de identidad del representante no puede quedar vacio"'},
    numericality: { only_integer: true, message: '"El numero de identidad del representante debe de ser numerico"'},
    uniqueness: { scope: :type_id, message: '"El numero y tipo de identidad del representante ya fue registrada"' }
    validates :name, presence: { message: '"El nombre del representante no puede quedar vacio"'},
    length: { in: 3..30 , message: '"El nombre del representante no puede ser menor a (3) ni mayor a (30) caracteres"'}
    validates :last_name, presence: { message: '"El apellido del representante no puede quedar vacio"'},
    length: { in: 3..30 , message: '"El apellido del representante no puede ser menor a (3) ni mayor a (30) caracteres"'}
    validates :gender, presence: { message: '"El sexo del representante no puede quedar vacio"'}
    validates :direction, presence: { message: '"El dirección del representante no puede quedar vacio"'}
    validates :date_of_birth, inclusion: { in: (Date.new(1900,1,1)..Date.today-10.years), message: "\"La fecha de nacimiento no puede ser mayor de #{Date.today-10.years} ni menor de #{Date.new(1900,1,1)}\"" }

end
