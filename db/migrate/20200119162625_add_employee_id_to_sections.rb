class AddEmployeeIdToSections < ActiveRecord::Migration[6.0]
  def change
    add_reference :sections, :employee, null: true, foreign_key: true
  end
end
