class AddParishIdToRepresentatives < ActiveRecord::Migration[6.0]
  def change
    add_reference :representatives, :parish, null: false, foreign_key: true, default: 1
  end
end
