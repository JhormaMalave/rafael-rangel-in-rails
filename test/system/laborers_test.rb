require "application_system_test_case"

class LaborersTest < ApplicationSystemTestCase
  setup do
    @laborer = laborers(:one)
  end

  test "visiting the index" do
    visit laborers_url
    assert_selector "h1", text: "Laborers"
  end

  test "creating a Laborer" do
    visit laborers_url
    click_on "New Laborer"

    fill_in "Description", with: @laborer.description
    click_on "Create Laborer"

    assert_text "Laborer was successfully created"
    click_on "Back"
  end

  test "updating a Laborer" do
    visit laborers_url
    click_on "Edit", match: :first

    fill_in "Description", with: @laborer.description
    click_on "Update Laborer"

    assert_text "Laborer was successfully updated"
    click_on "Back"
  end

  test "destroying a Laborer" do
    visit laborers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Laborer was successfully destroyed"
  end
end
