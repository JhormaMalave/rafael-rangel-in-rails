class AddRepresentativeIdToEnrollments < ActiveRecord::Migration[6.0]
  def change
    add_reference :enrollments, :representative, null: true, foreign_key: true
  end
end
