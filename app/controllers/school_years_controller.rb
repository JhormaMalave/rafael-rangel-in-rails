class SchoolYearsController < ApplicationController
  before_action :authenticate_user!

  def index
    @school_years = SchoolYear.all
  end
  def show
    @school_year = SchoolYear.find(params[:id])
  end
end
