class AddWorkingTimeToAdministratives < ActiveRecord::Migration[6.0]
  def change
    add_column :administratives, :working_time, :integer
  end
end
