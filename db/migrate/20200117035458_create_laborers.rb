class CreateLaborers < ActiveRecord::Migration[6.0]
  def change
    create_table :laborers do |t|
      t.text :description

      t.timestamps
    end
  end
end
