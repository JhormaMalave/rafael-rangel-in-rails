require "application_system_test_case"

class SchoolPeriodsTest < ApplicationSystemTestCase
  setup do
    @school_period = school_periods(:one)
  end

  test "visiting the index" do
    visit school_periods_url
    assert_selector "h1", text: "School Periods"
  end

  test "creating a School period" do
    visit school_periods_url
    click_on "New School Period"

    fill_in "Name", with: @school_period.name
    click_on "Create School period"

    assert_text "School period was successfully created"
    click_on "Back"
  end

  test "updating a School period" do
    visit school_periods_url
    click_on "Edit", match: :first

    fill_in "Name", with: @school_period.name
    click_on "Update School period"

    assert_text "School period was successfully updated"
    click_on "Back"
  end

  test "destroying a School period" do
    visit school_periods_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "School period was successfully destroyed"
  end
end
