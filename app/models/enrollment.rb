class Enrollment < ApplicationRecord
  belongs_to :school_year
  belongs_to :section, required: false
  belongs_to :student
  belongs_to :school_period
  belongs_to :representative, required: false
  has_many :notes

  enum status: [:Activo, :Retirado]

  validates_uniqueness_of :student_id, scope: :school_period, message: '"Este estudiante ya fue inscrito"'

  before_validation :generate_student_id
  before_validation :generate_representative_id

  
  attr_accessor :type_id_id
  attr_accessor :id_number
  attr_accessor :representative_type_id_id
  attr_accessor :representative_id_number 

  def generate_student_id
    student_search = Student.find_by(type_id: type_id_id, id_number: id_number)
    if student_search != nil
      self.student_id = student_search.id
    end
  end    
  def generate_representative_id
    representative_search = Representative.find_by(type_id: representative_type_id_id, id_number: representative_id_number )
    if representative_search != nil
      self.representative_id = representative_search.id
    end
  end
end
