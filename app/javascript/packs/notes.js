class notesController{

    notesIndex() {

    }

    notesNew () {
        this.yearId = document.getElementById('note_school_year_id');
        
        this.yearId.addEventListener('click', this.getSubjectsAndSection)

    }

    view () {

    }

    getSubjectsAndSection (e) {
    e.preventDefault()
        if(e.target.tagName === 'OPTION'){
            const idSchoolYear = e.target.value
            console.log(idSchoolYear)
            //Se obtiene las asignaturas de este año escolar
            let select = document.getElementById('note_subject_id');
            select.innerHTML = '';
            fetch(`/subjects/year/${idSchoolYear}.json`).
                then(result => result.json()).
                then(values => {
                name.disabled = false;
                const option = document.createElement('option')
                option.innerHTML = '- Seleccione -';
                option.value = '';
                select.appendChild(option);
                values.forEach(value => {
                    let option = document.createElement('option');
                    option.appendChild(document.createTextNode(value.name) );
                    option.value = value.id;
                    select.appendChild(option);
            });
            }).
            catch(error => console.log(error));

            //Se obtiene las secciones de este año escolar//Se obtiene las asignaturas de este año escolar
            let selectSection = document.getElementById('note_section_id');
            selectSection.innerHTML = '';
            fetch(`/sections/year/${idSchoolYear}.json`).
                then(result => result.json()).
                then(values => {
                name.disabled = false;
                const option = document.createElement('option')
                option.innerHTML = '- Seleccione -';
                option.value = '';
                selectSection.appendChild(option);
                values.forEach(value => {
                    let option = document.createElement('option');
                    option.appendChild(document.createTextNode(value.name) );
                    option.value = value.id;
                    selectSection.appendChild(option);
            });
            }).
            catch(error => console.log(error));
        }
    }
}


if(document.getElementById('form_note')){
    const notes = new notesController()
    notes.notesNew()
}

