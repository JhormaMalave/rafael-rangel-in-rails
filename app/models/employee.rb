class Employee < ApplicationRecord
    belongs_to :type_id
    belongs_to :parish
    belongs_to :workstation
    has_many :laborers
    has_many :administratives
    has_many :teachers
    has_many :sections

    enum gender: [:Masculino, :Femenino ]

    validates :id_number, presence: { message: '"El numero de identidad del empleado no puede quedar vacio"'},
    numericality: { only_integer: true, message: '"El numero de identidad del empleado debe de ser numerico"'},
    uniqueness: { scope: :type_id, message: '"El numero y tipo de identidad del empleado ya fue registrada"' }
    validates :name, presence: { message: '"El nombre del empleado no puede quedar vacio"'},
    length: { in: 3..30 , message: '"El nombre del empleado no puede ser menor a (3) ni mayor a (30) caracteres"'}
    validates :last_name, presence: { message: '"El apellido del empleado no puede quedar vacio"'},
    length: { in: 3..30 , message: '"El apellido del empleado no puede ser menor a (3) ni mayor a (30) caracteres"'}
    validates :gender, presence: { message: '"El sexo del empleado no puede quedar vacio"'}
    validates :direction, presence: { message: '"El dirección del empleado no puede quedar vacio"'}
    validates :date_of_birth, inclusion: { in: (Date.new(1900,1,1)..Date.today-10.years), message: "\"La fecha de nacimiento no puede ser mayor de #{Date.today-10.years} ni menor de #{Date.new(1900,1,1)}\"" }

end
