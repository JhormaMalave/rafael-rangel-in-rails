class Parish < ApplicationRecord
    belongs_to :municipality
    has_many :representatives
    has_many :employees
    has_many :students

end
