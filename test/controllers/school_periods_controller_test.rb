require 'test_helper'

class SchoolPeriodsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @school_period = school_periods(:one)
  end

  test "should get index" do
    get school_periods_url
    assert_response :success
  end

  test "should get new" do
    get new_school_period_url
    assert_response :success
  end

  test "should create school_period" do
    assert_difference('SchoolPeriod.count') do
      post school_periods_url, params: { school_period: { name: @school_period.name } }
    end

    assert_redirected_to school_period_url(SchoolPeriod.last)
  end

  test "should show school_period" do
    get school_period_url(@school_period)
    assert_response :success
  end

  test "should get edit" do
    get edit_school_period_url(@school_period)
    assert_response :success
  end

  test "should update school_period" do
    patch school_period_url(@school_period), params: { school_period: { name: @school_period.name } }
    assert_redirected_to school_period_url(@school_period)
  end

  test "should destroy school_period" do
    assert_difference('SchoolPeriod.count', -1) do
      delete school_period_url(@school_period)
    end

    assert_redirected_to school_periods_url
  end
end
