class CreateAdministratives < ActiveRecord::Migration[6.0]
  def change
    create_table :administratives do |t|
      t.text :description

      t.timestamps
    end
  end
end
