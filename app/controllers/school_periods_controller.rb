class SchoolPeriodsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_school_period, only: [:show, :edit, :update, :destroy, :change_school_period]

  # GET /school_periods
  # GET /school_periods.json
  def index
    @school_periods = SchoolPeriod.all
  end

  # GET /school_periods/1
  # GET /school_periods/1.json
  def show
  end

  # GET /school_periods/new
  def new
    @school_period = SchoolPeriod.new
  end

  # GET /school_periods/1/edit
  def edit
  end

  # POST /school_periods
  # POST /school_periods.json
  def create
    @school_period = SchoolPeriod.new(school_period_params)
    respond_to do |format|
      if @school_period.save
        format.html { redirect_to @school_period, notice: 'Período escolar creado exitosamente.' }
        format.json { render :show, status: :created, location: @school_period }
      else
        format.html { render :new }
        format.json { render json: @school_period.errors, status: :unprocessable_entity }
      end
    end
  end

  def change_school_period
    respond_to do |format|
      if session[:school_period] == @school_period.id
        format.html { redirect_to @school_period, notice: 'El período escolar ya fue seleccionado.' }
      else
        session[:school_period] = @school_period.id
        format.html { redirect_to @school_period, notice: 'El período escolar fue seleccionado exitosamente.' }
      end
    end
  end

  # PATCH/PUT /school_periods/1
  # PATCH/PUT /school_periods/1.json
  def update
    respond_to do |format|
      if @school_period.update(school_period_params)
        format.html { redirect_to @school_period, notice: 'Período escolar actualizado exitosamente.' }
        format.json { render :show, status: :ok, location: @school_period }
      else
        format.html { render :edit }
        format.json { render json: @school_period.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /school_periods/1
  # DELETE /school_periods/1.json
  def destroy
    respond_to do |format|
      if @school_period.id == session[:school_period]
        format.html { redirect_to school_periods_url, alert: 'No se puede eliminar el período escolar debido a que está en uso.' }
        format.json { head :no_content }
      else
        @school_period.destroy
        format.html { redirect_to school_periods_url, notice: 'Período escolar eliminado exitosamente.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_school_period
      @school_period = SchoolPeriod.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def school_period_params
      params.require(:school_period).permit(:name)
    end
end
