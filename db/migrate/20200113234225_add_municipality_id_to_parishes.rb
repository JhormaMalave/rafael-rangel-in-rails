class AddMunicipalityIdToParishes < ActiveRecord::Migration[6.0]
  def change
    add_reference :parishes, :municipality, null: false, foreign_key: true, default: 1
  end
end
